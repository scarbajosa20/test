resource "google_compute_instance" "caff-instances" {
 
  name         = "prueba"
  machine_type = "f1-micro"
  zone         = "europe-west1-b"

  tags = each.value.tags

  boot_disk {
    initialize_params {
      image = "ubuntu-minimal-1804-lts"
      size = 20
    }
  }

  // Local SSD disk

  network_interface {
    network ="default"

    access_config {
      // Ephemeral IP
    }
  }
